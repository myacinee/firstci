package fr.levitt.tp2.core.main;

import io.javalin.Javalin;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        //Javalin app = Javalin.create().start(7000);
        //app.get("/", ctx -> ctx.result("Hello World"));
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://api.navitia.io/v1/coverage/sncf/stop_areas/stop_area%3AOCE%3ASA%3A87444000/departures?from_datetime=20200303T144459&")
                .header("Authorization","Basic MDg1OWU0NTQtMWExNi00MWJiLWJjYWItOWQwMzRmMDU5NGMyOjA4NTllNDU0LTFhMTYtNDFiYi1iY2FiLTlkMDM0ZjA1OTRjMg==")
                .build();

        try {
            Response response = client.newCall(request).execute();
            System.out.println(response.code());
            System.out.println(response.body().string());
        }
        catch (IOException e) {
            System.out.println("Erreur réseau");
            e.printStackTrace();
        }


    }
}
