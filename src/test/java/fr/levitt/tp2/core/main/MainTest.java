package fr.levitt.tp2.core.main;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MainTest {

    @Test
    public void shouldAddNumbers() {
        int a = 2;
        int b = 3;

        int c = a + b;

        Assertions.assertEquals(c,a+b);
    }
}
